#include "solarixparser.h"
#include <memory.h>
#include "iostream"
#include <cstring>
#include "assert.h"
#include "vector"

const std::string underfined ="UNDERFINED";
const std::string nominative ="ИМ";
namespace solarixParser
{
    SyntaxParser::SyntaxParser()
    {
//        MorphologicalFlags |= SOL_GREN_MODEL;
    }
    HGREN_RESPACK SyntaxParser::Parse( HGREN hEngine, const char * sentence )
    {
        return sol_SyntaxAnalysis8( hEngine, sentence, MorphologicalFlags, SyntacticFlags, Constraints, Language );
    }

    MorphologyParser::MorphologyParser()
    {
//        flags |= SOL_GRE
    }
    HGREN_RESPACK MorphologyParser::Parse(HGREN hEngine, const char* sentence)
    {
        return sol_MorphologyAnalysis8( hEngine, sentence, MorphologicalFlags, SyntacticFlags, Constraints, RUSSIAN_LANGUAGE );
    }
void getAdv(HGREN hEngine, HGREN_TREENODE hNode,std::vector<std::string>& advs)
{
    char word[100];
    char name[100];
    char partOfSpeech[100];
    memset( word, 0, sizeof(word) );
    sol_GetNodeContents8( hNode, word );
//    std::cout<<word<<std::endl;
    int nver = sol_GetNodeVersionCount( hEngine, hNode );
    for(int ver = 0; ver<nver; ver++)
    {
//        int ver = 0;
        int id_entry = sol_GetNodeVerIEntry( hEngine, hNode, ver );
        int id_pos = -1;
        if( id_entry!=-1 )
        {
            id_pos = sol_GetEntryClass( hEngine, id_entry );
        }

        sol_GetEntryName8( hEngine, id_entry, name );

        sol_GetClassName8( hEngine, id_pos, partOfSpeech );
//        std::cout<<partOfSpeech<<std::endl;

        if(!std::strcmp(partOfSpeech,"ПРИЛАГАТЕЛЬНОЕ"))
        {
            advs.push_back(std::string(name));
        } else if(!std::strcmp(partOfSpeech,"СУЩЕСТВИТЕЛЬНОЕ"))
        {
            return;
        }
    }



    int nleaves = sol_CountLeafs( hNode );
    if( nleaves >0 )
    {
        for( int i=0; i<nleaves; i++ )
        {
            getAdv(hEngine, sol_GetLeaf( hNode, i ), advs);
        }

    }

}
std::string MorphParse( HGREN hEngine, HGREN_RESPACK hPack)
    {

    assert(hPack!=NULL );
    auto getParam = [] (HGREN hEngine, HGREN_TREENODE hToken, int j) ->
         std::string {
                 int id_coord = sol_GetNodePairCoord( hToken, j );
                 int id_state = sol_GetNodePairState( hToken, j );

                 char WordBuffer[100];
                 sol_GetCoordName8( hEngine, id_coord, WordBuffer );

                 char wordCase[20];
                 if( !sol_CountCoordStates( hEngine, id_coord )==0 )
                 {
                     sol_GetCoordStateName8( hEngine, id_coord, id_state, wordCase );
                     return std::string(wordCase);
                 } else
                 {
                     return underfined;
                 }
                     std::cout<<":";
                 };

     int n_linkage = sol_CountGrafs(hPack);
     if( n_linkage>0 )
      {
       int ntokens = sol_CountRoots(hPack,0);
       char WordBuffer[128];

       for( int i=1; i<ntokens-1; ++i )
       {
        HGREN_TREENODE hToken = sol_GetRoot( hPack, 0, i );

        *WordBuffer = 0;
        sol_GetNodeContents8( hToken, WordBuffer );

        int id_entry = sol_GetNodeIEntry( hEngine, hToken );
        int id_pos = sol_GetEntryClass( hEngine, id_entry );
        *WordBuffer = 0;
        sol_GetClassName8( hEngine, id_pos, WordBuffer );

        // get case of word
        auto wordCase = getParam(hEngine, hToken, 0);
        return wordCase;

//        int ntag = sol_GetNodePairsCount(hToken);

//        for( int j=0; j<ntag; ++j )
//         {
//            getParam(hEngine, hToken, j);
//         }
       }
      }

     return underfined;
    }
void bfs_node(HGREN hEngine, HGREN_TREENODE hNode, std::vector<Item>& items)
{

    char word[100];
    char partOfSpeech[100];
    memset( word, 0, sizeof(word) );
    sol_GetNodeContents8( hNode, word );

    ///todo:: why word two times
    //int nver = sol_GetNodeVersionCount( hEngine, hNode );
    int  nver = 1;
    for( int i=0; i<nver; ++i )
    {
        int id_entry = sol_GetNodeVerIEntry( hEngine, hNode, i );
        int id_pos = -1;
        if( id_entry!=-1 )
        {
            id_pos = sol_GetEntryClass( hEngine, id_entry );
        }
        word[0]=0;
        sol_GetEntryName8( hEngine, id_entry, word );
//        std::cout<<word<<' ';
        sol_GetClassName8( hEngine, id_pos, partOfSpeech );
//        std::cout<<partOfSpeech<<' ';
        ///--------- Morphologic
        if(!std::strcmp(partOfSpeech,"СУЩЕСТВИТЕЛЬНОЕ"))
        {
            solarixParser::MorphologyParser parser;
            auto hPack = parser.Parse(hEngine,word);
            std::string wordCase = MorphParse(hEngine,hPack);
            if(wordCase.compare(nominative) == 0)
            {
                std::vector<std::string> advs;
                getAdv(hEngine, hNode, advs);
                items.push_back(Item(std::string(word),advs));
            }
        }

    }

    int nleaves = sol_CountLeafs( hNode );
    if( nleaves >0 )
    {
        for( int i=0; i<nleaves; i++ )
        {
            bfs_node(hEngine, sol_GetLeaf( hNode, i ), items );
        }

    }

}

void bfs_grah(HGREN hEngine, HGREN_RESPACK hPack, int igraf, std::vector<Item>& items)
{
    int nroot = sol_CountRoots( hPack, igraf );

    for( int i=1; i<nroot-1; i++ )
    {
        bfs_node( hEngine, sol_GetRoot(hPack,igraf,i), items);
    }

}

std::vector<Item> bfs(HGREN hEngine, HGREN_RESPACK hPack)
{
    assert(hPack!=NULL);
    assert(hEngine!=NULL);
    std::vector<Item> items;
    const int ngrafs = sol_CountGrafs(hPack);

    if( ngrafs>0 )
     {
         bfs_grah( hEngine, hPack, 0, items);
     }

    return items;


}

}// namespace parser


