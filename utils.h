#ifndef UTILS_H
#define UTILS_H

#include "solarix_grammar_engine.h"
#include "_sg_api.h"
#include "stdio.h"
namespace solarixParser
{
    void SaveXML(const char * outpath, HGREN hEngine, HGREN_RESPACK hPack);
    void PrintError( HGREN hEngine );
}// namespace parser
#endif // UTILS_H
