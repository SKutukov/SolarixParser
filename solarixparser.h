#ifndef SOLARIXPARSER_H
#define SOLARIXPARSER_H

#include "solarixparser_global.h"

#include "string"
#include "solarix_grammar_engine.h"
#include "_sg_api.h"
#include "vector"
#include "interpretercore.h"

namespace solarixParser
{
    /*!
     * \brief The SyntaxParser class
     * Build syntax of  sentense
     */
    class SyntaxParser
    {
    private:
        int MorphologicalFlags = SOL_GREN_COMPLETE_ONLY | SOL_GREN_MODEL;
        int SyntacticFlags = SOL_GREN_REORDER_TREE;
        const int MAX_ALT = 15;
        const int TIMEOUT = 500;
        const int Constraints = (MAX_ALT<<22) | TIMEOUT;
        const int Language = RUSSIAN_LANGUAGE;
    public:
        /*!
         * \brief Parser
         */
        SyntaxParser();
        /*!
         * \brief Parse
         * Function for syntax parsing one sentence.
         * \param hEngine - ref to engine
         * \param sentence - sentence for parsing
         * \return
         */
        HGREN_RESPACK Parse( HGREN hEngine, const char* sentence );
    };
    /*!
     * \brief The MorphologyParser class
     * morphological analyze for each word in sentense
     */
    class MorphologyParser
    {
    private:
       int MorphologicalFlags = SOL_GREN_COMPLETE_ONLY | SOL_GREN_MODEL;
       int SyntacticFlags = 0;
       const int Constraints = 0;
       int language = RUSSIAN_LANGUAGE;
    public:
       /*!
        * \brief MorphologyParser
        */
       MorphologyParser();
       /*!
        * \brief Parse
        * Function for morphologic analyzing one sentence
        * \param hEngine - ref to engine
        * \param sentence - sentence for parsing
        */
       HGREN_RESPACK Parse(HGREN hEngine, const char* sentence );
    };
    /*!
     * \brief bfs
     * BFS for finding string in syntax tree
     * \param hEngine - endgine loaded from dictionary
     * \param hPack - syntax tree
     * \param string - string to find
     */
    std::vector<Item> bfs(HGREN hEngine, HGREN_RESPACK hPack);
    std::string MorphParse( HGREN hEngine, HGREN_RESPACK hPack);
    void getAdv(HGREN hEngine, HGREN_TREENODE hNode,std::vector<std::string>& advs);
}// namespace parser

#endif // SOLARIXPARSER_H
