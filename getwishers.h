#ifndef GETWISHERS_H
#define GETWISHERS_H

#include "vector"
#include "interpretercore.h"
#include "solarix_grammar_engine.h"
#include "_sg_api.h"

namespace solarixParser
{
    std::vector<Wisher> getWishers(HGREN hEngine, HGREN_RESPACK hPack, std::string dbName,
                               std::string username, std::string password,std::string host,std::string port);
}// namespace parser


#endif // GETWISHERS_H
