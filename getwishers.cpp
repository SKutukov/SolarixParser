#include "getwishers.h"
#include "memory"
#include <cstring>
#include "solarixparser.h"
#include "assert.h"
#include "dbconnector.h"

namespace solarixParser
{

    void saveWishTree(HGREN hEngine, HGREN_RESPACK hPack, HGREN_TREENODE hNode, std::vector<Item>& items, db::PostgreSQLConnector db)
    {
        char word[100];
        char name[100];
        char partOfSpeech[100];
        memset( word, 0, sizeof(word) );
        sol_GetNodeContents8( hNode, word );
        int nver = sol_GetNodeVersionCount( hEngine, hNode );
        if (nver >0)
        {
            int ver = 0;
            int id_entry = sol_GetNodeVerIEntry( hEngine, hNode, ver );
            int id_pos = -1;
            if( id_entry!=-1 )
            {
                id_pos = sol_GetEntryClass( hEngine, id_entry );
            }

            sol_GetEntryName8( hEngine, id_entry, name );
            sol_GetClassName8( hEngine, id_pos, partOfSpeech );
            if(!std::strcmp(partOfSpeech,"ИНФИНИТИВ") || !std::strcmp(partOfSpeech,"ГЛАГОЛ"))
            {
                return;
            } else if(!std::strcmp(partOfSpeech,"СУЩЕСТВИТЕЛЬНОЕ"))
            {
                auto k =db.findObject(std::string(name));
                if(k>=1)
                {
                    std::vector<std::string> advs;
                    int nleaves = sol_CountLeafs( hNode );
                    if( nleaves >0 )
                    {
                        for( int i=0; i<nleaves; i++ )
                        {
                            solarixParser::getAdv(hEngine, sol_GetLeaf( hNode, i ), advs);
                        }

                    }
                    items.push_back(Item(std::string(name),advs));
                }


            }
        }

        int nleaves = sol_CountLeafs( hNode );
        if( nleaves >0 )
        {
            for( int i=0; i<nleaves; i++ )
            {
                saveWishTree(hEngine, hPack, sol_GetLeaf( hNode, i ), items, db);
            }
        }
    }

    void SaveNode(HGREN hEngine, HGREN_RESPACK hPack, HGREN_TREENODE hNode,std::vector<Wisher>& wishers, db::PostgreSQLConnector db)
    {

        char word[100];
        char name[100];
        char partOfSpeech[100];
        memset( word, 0, sizeof(word) );
        sol_GetNodeContents8( hNode, word );
        int nver = sol_GetNodeVersionCount( hEngine, hNode );
        if( nver > 0)
        {
            int ver = 0;
            int id_entry = sol_GetNodeVerIEntry( hEngine, hNode, ver );
            int id_pos = -1;
            if( id_entry!=-1 )
            {
                id_pos = sol_GetEntryClass( hEngine, id_entry );
            }
            sol_GetEntryName8( hEngine, id_entry, name );
            sol_GetClassName8( hEngine, id_pos, partOfSpeech );
            if(!std::strcmp(partOfSpeech,"ИНФИНИТИВ") || !std::strcmp(partOfSpeech,"ГЛАГОЛ"))
            {
                std::vector<Item> items;
                int nleaves = sol_CountLeafs( hNode );
                if( nleaves >0 )
                {
                    for( int i=0; i<nleaves; i++ )
                    {
                        saveWishTree(hEngine, hPack, sol_GetLeaf( hNode, i ), items, db);
                    }

                }
                wishers.push_back(Wisher(std::string(word), items));
            }
        }

        int nleaves = sol_CountLeafs( hNode );
        if( nleaves >0 )
        {
            for( int i=0; i<nleaves; i++ )
            {
                SaveNode(hEngine, hPack, sol_GetLeaf( hNode, i ), wishers, db);
            }
        }

        return;
}

    void SaveGraph(HGREN hEngine, HGREN_RESPACK hPack, int igraf, std::vector<Wisher>& wishers, db::PostgreSQLConnector db )
    {
        int nroot = sol_CountRoots( hPack, igraf );

        for( int i=1; i<nroot-1; i++ )
        {
            SaveNode(hEngine, hPack, sol_GetRoot(hPack,igraf,i), wishers, db);
        }

        return;
    }

    void SaveSyntaxTrees(HGREN hEngine, HGREN_RESPACK hPack, std::vector<Wisher>& wishers, db::PostgreSQLConnector db )
    {
        assert(hPack != NULL);
        const int ngrafs = sol_CountGrafs(hPack);
        if( ngrafs>0 )
        {
            SaveGraph( hEngine, hPack, 0, wishers, db);
        }

        return;
    }


    std::vector<Wisher> getWishers(HGREN hEngine, HGREN_RESPACK hPack, std::string dbName,
                               std::string username, std::string password,std::string host,std::string port)
    {
        db::PostgreSQLConnector db(dbName, username, password, host, port);
        std::vector<Wisher> wishers;
        SaveSyntaxTrees(hEngine, hPack, wishers, db);
        return wishers;
    }
}// namespace parser
