#-------------------------------------------------
#
# Project created by QtCreator 2018-02-23T20:34:14
#
#-------------------------------------------------

QT       -= core gui

TARGET = SolarixParser
TEMPLATE = lib

DEFINES += SOLARIXPARSER_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        solarixparser.cpp \
    getwishers.cpp \
    utils.cpp

HEADERS += \
        solarixparser.h \
        solarixparser_global.h \ 
    getwishers.h \
    utils.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../InterpreterCore/release/ -lInterpreterCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../InterpreterCore/debug/ -lInterpreterCore
else:unix: LIBS += -L$$OUT_PWD/../InterpreterCore/ -lInterpreterCore

INCLUDEPATH += $$PWD/../InterpreterCore
DEPENDPATH += $$PWD/../InterpreterCore

INCLUDEPATH += $$PWD/../Interpreter

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib64/release/ -lsolarix_grammar_engine
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib64/debug/ -lsolarix_grammar_engine
else:unix: LIBS += -L$$PWD/lib64/ -lsolarix_grammar_engine

INCLUDEPATH += $$PWD/include/lem/solarix
DEPENDPATH += $$PWD/include/lem/solarix
