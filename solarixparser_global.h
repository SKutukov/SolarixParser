#ifndef SOLARIXPARSER_GLOBAL_H
#define SOLARIXPARSER_GLOBAL_H

#if defined(SOLARIXPARSER_LIBRARY)
#  define SOLARIXPARSERSHARED_EXPORT Q_DECL_EXPORT
#else
#  define SOLARIXPARSERSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // SOLARIXPARSER_GLOBAL_H
