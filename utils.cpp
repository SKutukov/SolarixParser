#include "utils.h"

#include <memory.h>
#include "iostream"
#include "assert.h"

namespace solarixParser
{

    void fprint_xml( FILE * f, const char * abuffer )
    {

        int l8 = strlen(abuffer);
        for( int i=0; i<l8; ++i )
        {
            char c = abuffer[i];
            if( c=='<' )
            {
                fprintf( f, "&lt;" );
            }
            else if( c=='>' )
            {
                fprintf( f, "&gt;" );
            }
            else if( c=='&' )
            {
                fprintf( f, "&amp;" );
            }
            else
            {
                fputc( c, f );
            }
        }


        return;
    }

    void PrintNode( FILE * out, HGREN hEngine, HGREN_RESPACK hPack, HGREN_TREENODE hNode )
    {
        fprintf( out, "<node>\n" );

        fprintf( out, "<lexeme position='%d'>", sol_GetNodePosition(hNode) );
        char word[100];
        memset( word, 0, sizeof(word) );
        sol_GetNodeContents8( hNode, word );
        fprint_xml( out, word );
        fprintf( out, "</lexeme>\n" );

        int nver = sol_GetNodeVersionCount( hEngine, hNode );
        fprintf( out, "<versions count='%d'>\n", nver );

        for( int i=0; i<nver; ++i )
        {
            int id_entry = sol_GetNodeVerIEntry( hEngine, hNode, i );
            int id_pos = -1;
            if( id_entry!=-1 )
            {
                id_pos = sol_GetEntryClass( hEngine, id_entry );
            }

           fprintf( out, "<version index='%d' id_entry='%d' id_pos='%d' lemma='", i, id_entry, id_pos );

           word[0]=0;
           sol_GetEntryName8( hEngine, id_entry, word );
           fprint_xml( out, word );
           fprintf( out, "' pos='" );
           //   std::cout<<word;
           word[0]=0;
           sol_GetClassName8( hEngine, id_pos, word );
           fprint_xml( out, word );
           //   std::cout<<word<<' '<<std::endl;
           fprintf( out, "'/>\n" );
        }

        fprintf( out, "</versions>\n" );

        int nleaves = sol_CountLeafs( hNode );
        if( nleaves >0 )
        {
            fprintf( out, "<children count='%d'>\n", nleaves );
            for( int i=0; i<nleaves; i++ )
            {
                PrintNode( out, hEngine, hPack, sol_GetLeaf( hNode, i ) );
            }

            fprintf( out, "</children>\n" );
        }

        fprintf( out, "</node>\n" );

        return;
    }
    void PrintGraph( FILE * out, HGREN hEngine, HGREN_RESPACK hPack, int igraf )
    {
     int nroot = sol_CountRoots( hPack, igraf );

     for( int i=1; i<nroot-1; i++ )
      {
       PrintNode( out, hEngine, hPack, sol_GetRoot(hPack,igraf,i) );
      }

     return;
    }

    void PrintSyntaxTrees( FILE * out, HGREN hEngine, HGREN_RESPACK hPack )
    {
     if( hPack!=NULL )
      {
       const int ngrafs = sol_CountGrafs(hPack);

       if( ngrafs>0 )
       {
            PrintGraph( out, hEngine, hPack, 0 );
       }
      }

     return;
    }

    void SaveXML(const char * outpath, HGREN hEngine, HGREN_RESPACK hPack)
    {
        FILE * out=NULL;
        out = fopen( outpath, "w" );
        assert(out != NULL);

        fprintf( out, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" );
        fprintf( out, "<dataroot>\n" );

        fprintf( out, "<dependencies>\n" );
        PrintSyntaxTrees( out, hEngine, hPack );
        fprintf( out, "</dependencies>\n" );

        fprintf( out, "</dataroot>\n" );

        fclose(out);
    }

    void PrintError( HGREN hEngine )
    {
        int err_len = sol_GetErrorLen(hEngine);
        char * err = new char[ err_len*6+1];
        sol_GetError8( hEngine, err, err_len+1 );
        sol_ClearError( hEngine );
        printf( "%s\n", err );

        return;
    }

}// namespace parser
